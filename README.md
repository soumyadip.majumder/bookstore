# BOOK STORE

This project is created for learning Purpose for Redux.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs all the Node Modules needed to run the application.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.