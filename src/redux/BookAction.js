import { buyBook,price } from "./BookType"

const purchase_book=()=>{
    return{
        type: buyBook
    }
}

const update_price=()=>{
    return {
        type: price
    }
}

export {purchase_book,update_price} 