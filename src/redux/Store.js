
import {createStore } from 'redux';
import Reducer from './BookReducer';

const store=createStore(Reducer);

export default store;
