import React from 'react'
import { useDispatch, useSelector} from 'react-redux'
import {purchase_book,update_price} from './BookAction'


function BookContainer() {
  const noOfBooks= useSelector(state=> state.NumberofBooks)
  const price= useSelector(state=> state.price)
  const dispatch = useDispatch()
  function handleClick(){
    dispatch(purchase_book());
    dispatch(update_price());
  }
  return (
    <>
        <div> BookContainer </div>
        <h2>No of Books - {noOfBooks}</h2>
        <h1>Price: {price}</h1>
        <button onClick={()=>handleClick()}>Buy Book</button>
    </>
  )
}

export default BookContainer
