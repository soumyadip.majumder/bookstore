import { buyBook,price } from "./BookType"

const initialState = {
    NumberofBooks:10,
    price: 200,
}

const Reducer=(state=initialState, action)=>{
    
    switch(action.type){
        case buyBook: return{
            ...state,   //previous state
            NumberofBooks: state.NumberofBooks-1   //updates state
        }
        case price: return {
            ...state,
            price: state.price-100
        }

        default: return state
    }
}


export default Reducer;